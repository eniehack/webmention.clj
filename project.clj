(defproject webmention "0.1.0-SNAPSHOT"
  :description "a library for discover webmention endpoint"
  :url "http://gitlab.com/eniehack/webmention.clj"
  :license {:name "Apache-2.0"
            :url "https://www.apache.org/licenses/LICENSE-2.0"}
  :dependencies [[org.clojure/clojure "1.10.3"]
                 [lambdaisland/uri "1.13.95"]
                 [clj-http "3.12.3"]]
  :repl-options {:init-ns webmention.core})
