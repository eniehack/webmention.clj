(ns webmention.core
  (:require [clj-http.client :as client]
            [lambdaisland.uri :refer [uri join relative?]]
            [webmention.discover :refer [pick-webmention-header default-user-agent]]))

(defn discover-endpoint
  ""
  [url-str & ua]
  (let [user-agent (if (seq ua)
                     ua
                     default-user-agent)
        url (uri url-str)
        link-header (-> url-str (client/head {:headers {"User-Agent" user-agent}}) :links pick-webmention-header rest first :href uri)]
    (if (nil? link-header)
      "html"
      (if (relative? link-header)
        (str (join url link-header))
        (str link-header)))))
