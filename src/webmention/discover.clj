(ns webmention.discover
  (:require [clojure.string :as str]))

(def default-user-agent "Webmention.clj/0.1.0 (+https://gitlab.com/eniehack/webmention.clj)")

(defn contain-webmention?
  ""
  [m]
  (str/includes? m "webmention"))

(defn webmention-header?
  "a predicate to check if it is a webmention header."
  [m]
  (-> m
      first
      contain-webmention?))

(defn pick-webmention-header
  "picks a header that contains webmention in Link headers"
  [m]
  (->> m (filter webmention-header?) first))

(defn webmention-tag?
  "a predicate to check if it is a webmention header."
  [m]
  (some #(= % m) [:a :meta]))

(defn pick-webmention-element
  [elem]
  {})
