(ns webmention.discover-test
  (:require [clojure.test :refer :all]
            [webmention.discover :refer :all]))

(deftest webmention-header?-test
  (testing "rel true case"
    (are [rel] (= true (webmention-header? [rel "https://example.com/webmention"]))
      "webmention"
      "webmention somerelation"
      "somerelation webmention"))
  (testing "rel false case"
    (are [rel] (= false (webmention-header? [rel "https://example.com/webmention"]))
      "something"
      "web"
      "webmantion")))


(deftest pick-webmention-header-test
  (testing ""
    (are [m] (= ["webmention" "https://example.com/webmention"] (pick-webmention-header m))
      {"webmention" "https://example.com/webmention"}
      {"webmention" "https://example.com/webmention", "test" "https://example.com"}
      {"test" "https://example.com", "webmention" "https://example.com/webmention"})))

(deftest webmention-tag?-test
  (testing "true"
    (is (true? (webmention-tag? :a)))
    (is (true? (webmention-tag? :meta)))))

(deftest pick-webmention-element-test
  (testing ""
    ;; (is (= {:type :element, :attrs {:href "foo", :rel "webmention"}, :tag :a, :content ["foo"]}
    ;;        (pick-webmention-element
    ;;         {:type :document, :content [{:type :element, :attrs nil, :tag :html, :content [{:type :element, :attrs nil, :tag :head, :content nil} {:type :element, :attrs nil, :tag :body, :content [{:type :element, :attrs {:href "foo", :rel "webmention"}, :tag :a, :content ["foo"]}]}]}]})))
    (is (= {} (pick-webmention-element {})))))
