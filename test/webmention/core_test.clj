(ns webmention.core-test
  (:require [clojure.test :refer :all]
            [webmention.core :refer :all]))

(deftest webmention-rocks-discovery-test-1
  (testing "Discovery Test #1"
    (is (= "https://webmention.rocks/test/1/webmention?head=true" (discover-endpoint "https://webmention.rocks/test/1"))))
  (testing "Discovery Test #2"
    (is (= "https://webmention.rocks/test/2/webmention?head=true" (discover-endpoint "https://webmention.rocks/test/2"))))
  (testing "Discovery Test #3"
    (is (= "https://webmention.rocks/test/3/webmention" (discover-endpoint "https://webmention.rocks/test/3")))))
